const express = require("express");
const router = express.Router();
const User = require("../models/User.js");
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.patch("/setAsAdmin/:userId", auth.verify, (req, res) => {
	let data = {
		user : auth.decode(req.headers.authorization), //current user logged-in
		userId : req.params.userId //user to be updated to admin
	}

	userController.toAdmin(data).then(resultFromController => res.send(resultFromController))
})

router.post("/userDetails", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getUser({userId : userData.id}).then(resultFromController => res.send(resultFromController))
})

router.get("/", auth.verify, (req, res) => {
	userController.getUsers().then(resultFromController => res.send(resultFromController))
})

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;