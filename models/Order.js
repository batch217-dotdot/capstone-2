const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
		type : String,
		required : [true, "UserId is required!"]
	},
	products : [{
		productId : {
			type : String,
			required : [true, "ProductId is required!"]
		},
		quantity : {
			type : Number,
			required : [true, "Quantity is required!"]
		}
	}],
	totalAmount : {
		type : Number,
		required : [true, "TotalAmount is required!"]
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	}
})

module.exports = mongoose.model("Order", orderSchema);