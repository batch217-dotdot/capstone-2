const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			let newUser = new User({
				email : reqBody.email,
				password : bcrypt.hashSync(reqBody.password, 10)
			})

			return newUser.save().then((user, error) => {
				if(error) {
					return false;
				}
				else {
					return true;
				}
			})
		}	
		else{
			return false
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				// generata an access 
				return {access : auth.createAccessToken(result)}
			}
			else{
				return false
			}
		}
	})
}

module.exports.toAdmin = (data) => {
	return User.findOne({_id : data.userId}).then(result => {
		if(result == null){
			return false
		}
		else{
			if(data.user.isAdmin == true){
				return User.findByIdAndUpdate(data.userId, {isAdmin : true})
				.then((result, error) => {
					if(error){
						return false
					}
					else{
						return User.findOne({_id : data.userId})
						.then((result, error) => {
							if(error) {
								return false
							}
							else{
								return true
							}
						})
					}
				})
			}
			else{
				return false
			}
		}
	})
}

module.exports.getUser = (data) => {
	return User.findById(data.userId).then(result => {
		return result;
	})
}

module.exports.getUsers = () => {
	return User.find({},{password : 0}).then(result => {
		if(result == null){
			return false
		}
		else{
			return result
		}
	})
}