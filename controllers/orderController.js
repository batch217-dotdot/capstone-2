const Product = require("../models/Product.js");
const User = require("../models/User.js");
const Order = require("../models/Order.js");


module.exports.createOrder = (data) => {
	return User.findById(data.user.id).then(result => {
		if(result == null){
			return false
		}
		else{
			if(result.isAdmin){
				return false
			}
			else{
				return Product.findById(data.products.productId).then(result => {
					if(result == null){
						return false
					}
					else{
						let newOrder = new Order({
							userId : data.user.id,
							products : {
								productId : data.products.productId,
								quantity : data.products.quantity
							},
							totalAmount : result.price * data.products.quantity
						})
						return newOrder.save().then((order, error) => {
							if(error){
								return false
							}
							else{
								return true
							}
						})
					}
				})
			}
		}
	})
}

module.exports.getAllOrders = (data) => {
	return User.findOne({_id : data.id}).then(result => {
		if(result == null){
			return false
		}
		else{
			if(data.isAdmin){
				return Order.find().then((result, error) => {
					if(error){
						return false
					}
					else if(result == null){
						return false
					}
					else{
						return result
					}
				})
			}
			else{
				return false
			}
		}
	})
}

module.exports.getUserOrders = (data) => {
	return User.findOne({_id : data.id}).then(result => {
		if(result == null){
			return false
		}
		else{
			if(data.isAdmin){
				return false
			}
			else{
				return Order.find({userId : data.id}).then((result, error) => {
					if(error){
						return false
					}
					else if(result == null){
						return false
					}
					else{
						return result
					}
				})
			}
		}
	})
}