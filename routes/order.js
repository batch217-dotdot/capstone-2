const express = require("express");
const router = express.Router();
const Order = require("../models/Order.js");
const orderController = require("../controllers/orderController.js");
const auth = require("../auth.js");


router.post("/checkout", auth.verify, (req, res) => {
	let data = {
			user : auth.decode(req.headers.authorization),
			products : req.body.products
		}
	
	orderController.createOrder(data).then(resultFromController => res.send(resultFromController))
})

router.get("/", auth.verify, (req, res) => {
	let data = auth.decode(req.headers.authorization)
	
	orderController.getAllOrders(data).then(resultFromController => res.send(resultFromController))
})

router.get("/userOrders", auth.verify, (req, res) => {
	let data = auth.decode(req.headers.authorization)
	
	orderController.getUserOrders(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;