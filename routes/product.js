const express = require("express");
const router = express.Router();
const Product = require("../models/Product.js");
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");


router.post("/addProduct", auth.verify, (req, res) => {
	let data = {
		user : auth.decode(req.headers.authorization),
		content : {
			name : req.body.name,
			description : req.body.description,
			price : req.body.price
		}
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController))
})

router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/", (req, res) => {
	productController.getProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/:productId", (req, res) => {
	productController.getSpecificProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
})

router.put("/:productId", auth.verify, (req, res) => {
	let data = {
		user : auth.decode(req.headers.authorization),
		content : {
			productId : req.params.productId,
			name : req.body.name,
			description : req.body.description,
			price : req.body.price
		}
	}

	productController.updateProduct(data).then(resultFromController => res.send(resultFromController))
})

router.patch("/:productId/archive", auth.verify, (req, res) => {
	let data = {
		user : auth.decode(req.headers.authorization),
		productId : req.params.productId
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
})

router.patch("/:productId/unarchive", auth.verify, (req, res) => {
	let data = {
		user : auth.decode(req.headers.authorization),
		productId : req.params.productId
	}

	productController.unarchiveProduct(data).then(resultFromController => res.send(resultFromController))
})


module.exports = router;